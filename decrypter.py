from __future__ import print_function
import aes
import glob2
import os
import sys
import magic
import zipfile
import shutil
import epub
import tempfile
from simplecrypt import SimpleCrypt

if sys.version_info < (3,):
    range = xrange

def getSecretKey(deviceid, path, use_simplecrypt=False):
    key = deviceid[:16]
    with open(path, 'rb') as r:
        if use_simplecrypt:
            crypto = SimpleCrypt(int(key.replace('-', ''), 16))
            buf = crypto.decrypt_to_bytes(r.read())
            decrypted = aes.decrypt(key, buf).decode('utf-8')
        else:
            try:
                decrypted = aes.decrypt(key, r.read()).decode('utf-8')
            except UnicodeDecodeError:
                decrypted = ''
        if decrypted[:len(key)] != key:
            print(deviceid, decrypted)
            raise TypeError("Wrong device id")
        return decrypted[len(deviceid) + 32:][:16]
    print(path)
    raise TypeError("Not found data file")

def decrypt_content(key, path, DEBUG=True):
    DEBUG and sys.stdout.write("try " + path + " ")
    filetype = magic.from_file(path).split()[0]
    if filetype in ['XML', 'ASCII', 'UTF-8', 'EPUB']:
        DEBUG and print("skip already decrypted " + filetype)
        return
    buf = None
    with open(path, 'rb') as r:
        encrypted = r.read()
        buf = aes.decryptWithPadding(key, encrypted)
    with open(path, 'wb') as w:
        w.write(buf)
    DEBUG and print("done")

def decrypt_cbc(key, path, DEBUG=True):
    DEBUG and sys.stdout.write("try " + path + " ")
    filetype = magic.from_file(path).split()[0]
    if filetype in ['PDF']:
        DEBUG and print("\tskip already decrypted " + filetype)
        return
    buf = []
    with open(path, 'rb') as r:
        encrypted = r.read()
        for idx, block_idx in enumerate(range(len(key), len(encrypted),
                                               len(key))):
            iv_idx = idx * len(key)
            iv = encrypted[iv_idx:iv_idx + len(key)]
            block = encrypted[block_idx:block_idx + len(key)]
            buf.append(aes.decrypt_cbc(key, iv, block))
    with open(path, 'wb') as w:
        for b in buf:
            w.write(b)
    DEBUG and print("done")

def decrypt(key, path, DEBUG=True):
    DEBUG and sys.stdout.write("try " + path + " ")
    filetype = magic.from_file(path).split()[0]
    if filetype in ['JPEG', 'PNG']:
        DEBUG and print("\tskip already decrypted " + filetype)
        return
    buf = None
    with open(path, 'rb') as r:
        encrypted = r.read()
        buf = aes.decrypt(key, encrypted)
    with open(path, 'wb') as w:
        w.write(buf)
    DEBUG and print("done")

def decrypt_zip(key, path, DEBUG=True):
    DEBUG and sys.stdout.write("try " + path + " ")
    tmpDirname = tempfile.mkdtemp(dir=os.path.dirname(path))
    #if not created temp dir then create it
    if os.path.exists(tmpDirname) == False:
        os.mkdir(tmpDirname)
    #read non-encrypted zip file and extract.
    zf = zipfile.ZipFile(path, 'r')
    fileList = []
    for zFileName in zf.namelist():
        zf.extract(zFileName, tmpDirname)
        fileList.append(zFileName)
    zf.close()
    os.remove(path)

    #decrypt origial img file and zipping,,,
    zf = zipfile.ZipFile(path, 'w')
    for wFileName in fileList:
        filePath = os.path.join(tmpDirname, wFileName)
        decrypt(key, filePath, DEBUG=DEBUG)
        zf.write(filePath, wFileName)
    zf.close()

    #remove all extracted temp file and done
    shutil.rmtree(tmpDirname)
    DEBUG and print("done")

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Ridibooks DRM Cracker')
    parser.add_argument('deviceid', metavar='DEVICEID', type=str,
                        help='client uuid')
    parser.add_argument('encrypted_dir', metavar='ENCRYPT_DIR', type=str,
                        help='cracking target directory')
    parser.add_argument('--use-simplecrypt', default=False,
                        action='store_true',
                        help='no use SimpleCrypt algorithm')

    args = parser.parse_args()
    book_dat = glob2.glob(args.encrypted_dir + '/*.dat')
    book_dat = len(book_dat) and book_dat[0]
    if not book_dat:
        print("not have .dat file")
        raise SystemExit
    key = getSecretKey(args.deviceid, book_dat, args.use_simplecrypt)
    img_key = args.deviceid[2:18]
    print("found key: " + key)

    types = (
        ('html', key, decrypt_content),
        ('xhtml', key, decrypt_content),
        ('zip', img_key, decrypt_zip),
        ('epub', key, decrypt_content),
        ('txt', key, decrypt_content),
        ('jpg', img_key, decrypt),
        ('png', img_key, decrypt),
        ('pdf', key, decrypt_cbc),
    )
    for ext, decrypt_key, decrypt_func in types:
        for filename in glob2.glob(args.encrypted_dir + '/**/*.' + ext):
            if filename[-13:] == 'thumbnail.png':
                continue
            decrypt_func(decrypt_key, filename)
