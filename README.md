# Ridibooks DRM Decrypter
This project started since 2012 Korean thanksgiving day.

## Changes
- 2012-10: Initial implements for android app data
- 2013-02: Support BOM books; for example comic book
- 2013-11: Move to windows application from android package
- 2014-02: Support python3
- 2015-03: Add auto runner
- 2015-12: Support PDF file
- 2016-08: Support Ridibooks 1.8.2; Introduce QTSimpleCrypt
- 2018-01: Support Ridibooks 2.3.0; Changed DB schema
- 2018-12: Support Ridibooks 2.5.5; Removed QTSimpleCrypt and changed key fetching method
- 2019-04: Support Ridibooks 2.6.1; Initial implement PyQJson port

## DRM algorithm
TBD

It didn't changed since very first time(2012)

## How to get device key
A traditional way can find the key from device page in ridibooks site.
but this method was removed after Brian's [blog post][bpak-ridi-drm-decrypt]

So we should use another methods

1. sniffing packet

    A client application would be sending the `device_id` to server multiple times

    1. bind new device
    2. download books
    3. ...

    if you dump the packet using sniffing tool, it would be easier method.

2. reverse and reimplement decrypt algorithm

    A client application still use registry section for storing device data
    (it means Ridibooks app used default QSetting storage),
    just changed algorithm after blog post.

    `device_id.py` is the script that reimplement this algorithm. but it couldn't be
    perfect, and I didn't implement key generation logic.
    but dumping key is very easy if you use any debugger in windows system.

    If you already know your `device_id`, backup it.
    Storing algorithm can change in the next version.

    This script was tested in `linux`, `python3 + windows` and `bash for windows`
    Sadly, didn't work on python2 + windows.

[bpak-ridi-drm-decrypt]: https://www.bpak.org/blog/2018/04/%EB%A6%AC%EB%94%94%EB%B6%81%EC%8A%A4-%EC%9E%90%EC%8B%A0%EC%9D%B4-%EC%86%8C%EC%9C%A0%ED%95%9C-%EC%B1%85-drm-%ED%95%B4%EC%A0%9C%ED%95%98%EA%B8%B0-feat-%EC%9C%84%ED%97%98%ED%95%9C-%EB%B9%84%EB%B0%80/
