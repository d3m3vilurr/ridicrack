import platform
import binascii
import sys
import base64
import aes
import zlib
import qjson

REG_GROUP = 'device'
REG_FULL_PATH = 'Software\RIDI\Ridibooks\{}'.format(REG_GROUP)
REG_KEY = 'device_id'

D0 = b'44700786d8c27b8da91ef8c691caf617'
K0 = b'92BD75E8D8FD2B0179172217B15350CC6D4002F0'
K1 = b'905CB326C779F0123E079BF8CCB223176A3CF32D'

# move set 2/3
#D1 = b'44700786d8c2device/device_idf617'
K2 = b'9244700786d8c2device/device_idf6174002F0'

# move set 1/2
#D2 = b'44700786d8c27bdevice/device_id17'
K3 = b'9044700786d8c27bdevice/device_id173CF32D'

#TABLE = (3, 8, 9, 10, 11, 5, 6, 7, 0, 1, 2, 4, 12, 13, 14, 15)

def is_bytearray(inp):
    return inp[:11] == b'@ByteArray(' and inp[-1:] == b')'

def strip_bytearray(inp):
    if is_bytearray(inp):
        # strip magic
        inp = inp[11:-1]
    return inp

def device_id():
    '''
    decrypt device id from QSetting

    pseudo encrypt steps:

        QByteArray encrypt(const char* device_id) {
            a = AES_ECB_ENC(K3, device_id)
            b = b64encode(a)
            c = QJson({d: b})
            d = qCompress(c)
            e = AES_ECB_ENC(K2, d)
            return QByteArray(e)
        }
    '''

    # HACK: for non-utf system
    if sys.getdefaultencoding() != 'utf-8':
        reload(sys)
        sys.setdefaultencoding('utf-8')
    plat = platform.system().lower()
    if plat == 'windows':
        import winreg

        reg = winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, REG_FULL_PATH)
        value, value_type = winreg.QueryValueEx(reg, REG_KEY)
        if value_type == winreg.REG_SZ:
            data = value.strip().encode('latin')
        elif value_type == winreg.REG_BINARY:
            data = value.decode('utf-16').encode('latin')
        else:
            raise IOError('Unsupport registry type')
    elif plat == 'linux':
        import os
        import subprocess

        # HACK: for `bash for windows`
        if os.path.exists('/mnt/c/Windows/System32/cmd.exe'):
            cmd = ['/mnt/c/Windows/System32/cmd.exe', '/c']
        else:
            cmd = ['wine']

        WINE_REG_FULL_PATH = 'HKEY_CURRENT_USER\{}'.format(REG_FULL_PATH)
        REG_QUERY_CMD = cmd + ['reg', 'query', WINE_REG_FULL_PATH, '/v', REG_KEY]

        # read registry value
        proc = subprocess.Popen(REG_QUERY_CMD, stdout=subprocess.PIPE)
        out, err = proc.communicate()
        if b'REG_BINARY' in out:
            value = out.split(b'REG_BINARY')[-1].strip()
            value = binascii.unhexlify(value)
            data = value.decode('utf-16').encode('latin')
        elif b'REG_SZ' in out:
            # FIXME: only test passed with `bash for windows`
            # in linux system, it occurs an error  when zlib.decompress
            data = out.split(b'REG_SZ')[-1].strip().decode().encode('latin')
        else:
            #REG_FILE = "ridi.reg"
            #REG_EXPORT_CMD = cmd + ['reg', 'export', WINE_REG_FULL_PATH, REG_FILE, '/y']
            #
            # read registry value
            #proc = subprocess.Popen(REG_EXPORT_CMD, stdout=subprocess.PIPE)
            #out, err = proc.communicate()
            #proc.communicate()
            #
            #with open(REG_FILE, 'r', encoding='utf-16') as f:
            #    buf = f.read().encode('latin')
            #    value = buf.split(b'device_id"=')[1].strip()
            #    print(value)

            raise IOError('Unsupport registry type')
    #elif plat == 'darwin':
    #    # TODO
    #    pass
    else:
        raise SystemError('Unsupported system')
    if not is_bytearray(data):
        raise IOError('Invalid input')

    data = strip_bytearray(data)
    data = aes.decrypt(K2[:16], data)
    # expected_size = data[:4]
    data = zlib.decompress(data[4:])
    json = qjson.QJson(data).parse()
    data = base64.b64decode(json['d'])
    data = aes.decrypt(K3[:16], data)
    return data.split(b'\x00')[0].decode()

if __name__ == '__main__':
    print(device_id())
