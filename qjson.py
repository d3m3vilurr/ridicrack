# Copyright (c) 2019, Sunguk Lee
#
# Original C++ Copyright (C) 2016 The Qt Company Ltd.
# Contact: https://www.qt.io/licensing/
#
# This file is part of the QtCore module of the Qt Toolkit.
#
# Commercial License Usage
# Licensees holding valid commercial Qt licenses may use this file in
# accordance with the commercial license agreement provided with the
# Software or, alternatively, in accordance with the terms contained in
# a written agreement between you and The Qt Company. For licensing terms
# and conditions see https://www.qt.io/terms-conditions. For further
# information use the contact form at https://www.qt.io/contact-us.
#
# GNU Lesser General Public License Usage
# Alternatively, this file may be used under the terms of the GNU Lesser
# General Public License version 3 as published by the Free Software
# Foundation and appearing in the file LICENSE.LGPL3 included in the
# packaging of this file. Please review the following information to
# ensure the GNU Lesser General Public License version 3 requirements
# will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
#
# GNU General Public License Usage
# Alternatively, this file may be used under the terms of the GNU
# General Public License version 2.0 or (at your option) the GNU General
# Public license version 3 or any later version approved by the KDE Free
# Qt Foundation. The licenses are as published by the Free Software
# Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
# included in the packaging of this file. Please review the following
# information to ensure the GNU General Public License requirements will
# be met: https://www.gnu.org/licenses/gpl-2.0.html and
# https://www.gnu.org/licenses/gpl-3.0.html.

import ctypes
import struct
from enum import Enum

__all__ = ['QJson']

class Base(object):
    def __init__(self, buf=None):
        self._buf = buf
        if self._buf:
            self._internal = self._Structure.from_buffer_copy(buf)
        else:
            self._internal = self._Structure()

    @property
    def _Structure(self):

        class InternalStructure(ctypes.Structure):
            pass

        return InternalStructure

    def __getattr__(self, key):
        return getattr(self._internal, key)

    def size(self):
        return ctypes.sizeof(self._internal)

class QJsonBase(Base):
    @property
    def _Structure(self):

        class _QJsonBase(ctypes.Structure):
            _fields_ = [('_size', ctypes.c_uint),
                        ('_dummy', ctypes.c_uint),
                        ('table_offset', ctypes.c_uint)]

            @property
            def is_object(self):
                return (self._dummy & 1) and True or False

            @property
            def is_array(self):
                return not self.is_object

            @property
            def length(self):
                return self._dummy >> 1

        return _QJsonBase

    @property
    def tables(self):
        buf = self._buf[self._internal.table_offset:]
        return list(map(lambda idx: struct.unpack('<I', buf[idx:idx + 4])[0],
                        range(0, len(buf), 4)))


class QJsonObjectBase(QJsonBase):
    def entry_at(self, idx):
        buf = self._buf[self.tables[idx]:]
        return QJsonEntry(buf, self._buf)

    def entries(self):
        return [self.entry_at(idx) for idx, _ in enumerate(self.tables)]

    def parse(self):
        ret = {}
        for entry in self.entries():
            ret[entry.key] = entry.value
        return ret


class QJsonArrayBase(QJsonBase):
    def at(self, idx):
        buf = self._buf[self.tables[idx]:]
        return QJsonValue(buf, self._buf)

    def items(self):
        return [self.at(idx) for idx, _ in enumerate(self.tables)]

    def parse(self):
        ret = []
        for item in self.items():
            ret.append(item.value())
        return ret


class QJsonValueType(Enum):
    NULL = 0
    BOOL = 1
    DOUBLE = 2
    STRING = 3
    ARRAY = 4
    OBJECT = 5
    UNDEFINED = 0x80

class QJsonValue(Base):
    def __init__(self, buf, base):
        super(QJsonValue, self).__init__(buf)
        self._base = base

    @property
    def _Structure(self):

        class _QJsonValue(ctypes.Structure):
            _fields_ = [('_dummy', ctypes.c_uint)]

            @property
            def type(self):
                return QJsonValueType(self._dummy & 0x7)

            @property
            def latin_or_int(self):
                return (self._dummy & 0x8) and 1 or 0

            @property
            def latin_key(self):
                return (self._dummy & 0x10)

            @property
            def _value(self):
                return self._dummy >> 5

        return _QJsonValue

    def value(self):
        if self.type == QJsonValueType.NULL:
            return None
        if self.type == QJsonValueType.BOOL:
            return self.to_bool()
        if self.type == QJsonValueType.DOUBLE:
            return self.to_double()
        if self.type == QJsonValueType.STRING:
            return self.to_string().decode()
        if self.type == QJsonValueType.ARRAY:
            pass
        if self.type == QJsonValueType.Object:
            pass
        if self.type == QJsonValueType.UNDEFINED:
            pass

    def to_string(self):
        assert(self.type == QJsonValueType.STRING)
        buf = self._base[self._value:]
        if self.latin_or_int:
            return QJsonLatin1String(buf)
        else:
            return QJsonString(buf)

    def to_double(self):
        assert(self.type == QJsonValueType.DOUBLE)
        if self.latin_or_int:
            return self._value
        # TODO

    def to_bool(self):
        assert(self.type == QJsonValueType.BOOL)
        return self._value != 0


class QJsonString(Base):
    @property
    def _Structure(self):

        class _QJsonString(ctypes.Structure):
            _fields_ = [('length', ctypes.c_uint)]

        return _QJsonString

    @property
    def utf16(self):
        offset = ctypes.sizeof(self._internal)
        length = ctypes.sizeof(ctypes.c_ushort) * self.length
        return self._buf[offset:offset + length]

    def decode(self):
        return self.utf16.decode('utf16')

    def byte_size(self):
        return ctypes.sizeof(self._internal) + len(self.utf16)


class QJsonLatin1String(Base):
    @property
    def _Structure(self):

        class _QJsonLatin1String(ctypes.Structure):
            _fields_ = [('length', ctypes.c_ushort)]

        return _QJsonLatin1String

    @property
    def latin1(self):
        offset = ctypes.sizeof(self._internal)
        length = ctypes.sizeof(ctypes.c_char) * self.length
        return self._buf[offset:offset + length]

    def decode(self):
        return self.latin1.decode('latin1')

    def byte_size(self):
        return ctypes.sizeof(self._internal) + len(self.latin1)


class QJsonEntry(Base):
    def __init__(self, buf, base):
        super(QJsonEntry, self).__init__(buf)
        self._value = QJsonValue(buf, base)

    @property
    def key(self):
        buf = self._buf[self._value.size():]
        if self._value.latin_key:
            return QJsonLatin1String(buf).decode()
        else:
            return QJsonString(buf).decode()

    @property
    def value(self):
        return self._value.value()


class QJson(Base):
    def __init__(self, buf=None):
        super(QJson, self).__init__(buf)
        self._root = None

    @property
    def _Structure(self):

        class _QJsonHeader(ctypes.Structure):
            _fields_ = [('tag', ctypes.c_uint),
                        ('version', ctypes.c_uint)]

            # TODO:verify
            def verify(self):
                pass

        return _QJsonHeader

    @property
    def root(self):
        buf = self._buf[ctypes.sizeof(self._internal):]
        base = QJsonBase(buf)
        if base.is_object:
            return QJsonObjectBase(buf)
        elif base.is_array:
            return QJsonArrayBase(buf)

    def parse(self):
        return self.root.parse()


if __name__ == '__main__':
    import sys

    with open(sys.argv[1], 'rb') as f:
        buf = f.read()
        json = QJson(buf)
        print(json.parse())
