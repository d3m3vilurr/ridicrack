from Crypto.Cipher import AES

def decrypt(key, stream):
    return _decrypt(key, stream)

def decryptWithPadding(key, stream):
    decrypted = _decrypt(key, stream, usePadding=True)
    padding = ord(decrypted[-1:])
    if padding > len(key):
        return decrypted
    if padding:
        decrypted = decrypted[:-padding]
    return decrypted

def _decrypt(key, stream, usePadding=False):
    cipher = AES.new(key, AES.MODE_ECB)
    stream = stream[:int(len(stream) / len(key)) * len(key)]
    decrypted = cipher.decrypt(stream)
    return decrypted

def decrypt_cbc(key, iv, stream):
    cipher = AES.new(key, AES.MODE_CBC, iv)
    stream = stream[:int(len(stream) / len(key)) * len(key)]
    decrypted = cipher.decrypt(stream)
    return decrypted
