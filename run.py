from __future__ import print_function
import os
import shutil
import sys
import glob
import sqlite3
import decrypter

RIDI_DATABASE = 'library.dat'


def main(device_id, db_path, lib_path, output):
    img_key = device_id[2:18]

    def decrypt_book(bookid, booktype):
        dat_file = os.path.join(lib_path, bookid, bookid + '.dat')
        if not os.path.exists(dat_file):
            print('not found dat file')
            return None, None
        sec_key = decrypter.getSecretKey(device_id, dat_file)
        if booktype == 0:
            key = img_key
            ext = 'zip'
            decrypt = decrypter.decrypt_zip
        elif booktype == 1:
            key = sec_key
            ext = 'epub'
            decrypt = decrypter.decrypt_content
        elif booktype == 2:
            key = sec_key
            ext = 'pdf'
            decrypt = decrypter.decrypt_cbc
        else:
            print('unknown booktype', booktype)
            return None, None
        orig = os.path.join(lib_path, bookid, bookid + '.' + ext)
        if booktype == 0 and not os.path.exists(orig):
            return decrypt_book(bookid, 1)
        filepath = orig + '.work'
        shutil.copy(orig, filepath)
        decrypt(key, filepath, DEBUG=False)
        return filepath, ext

    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    stmt = cursor.execute('''
        select id,
               title,
               author,
               format,
               group_title as series_title,
               group_type as series_type,
               order_in_group as volume
          from book left join bookgroup
            on book.group_key = bookgroup.primary_key
    ''')
    for bid, title, author, btype, series_title, series_type, volume \
            in stmt.fetchall():
        if not volume:
            info = dict(title=title, author=author)
            fmts = dict(screen=u'{title} - {author}',
                        filename=u'{title}.{ext}')
        elif series_type == 'series':
            info = dict(title=series_title or title, volume=volume,
                        author=author)

            _ = title.rsplit(' ', 1)
            if len(_) == 2 and _[-1].isdigit():
                volume_order = int(_[-1])
                info['volume_order'] = '-{:02d}'.format(volume_order)
            else:
                info['volume_order'] = ''

            fmts = dict(screen=u'{title} {volume:02d}{volume_order} - {author}',
                        filename=u'{title} {volume:02d}{volume_order}.{ext}')
        else:
            info = dict(series=series_title or 'unknown',
                        title=title, volume=volume, author=author)
            fmts = dict(screen=u'{series} {volume:02d}: {title} - {author}',
                        filename=u'{series} {volume:02d} - {title}.{ext}')
        print(fmts['screen'].format(**info))
        in_path, ext = decrypt_book(bid, btype)
        if not in_path:
            continue
        info['ext'] = ext
        out_fn = fmts['filename'].format(**info)
        valid_out_fn = "".join(i for i in out_fn if i not in "\/:*?<>|")
        out_path = os.path.join(output, valid_out_fn)
        shutil.move(in_path, out_path)

if __name__ == '__main__':
    import config
    db_path = os.path.join(config.LIBRARY_PATH, RIDI_DATABASE)
    if not os.path.exists(db_path):
        print("not found database file")
        raise SystemExit
    if not os.path.exists(config.OUTPUT_PATH):
        os.makedirs(config.OUTPUT_PATH, 0o775)
    main(config.DEVICE_ID, db_path, config.LIBRARY_PATH, config.OUTPUT_PATH)
